// PhD Simulation
//
// It might work with random walk but not with real cusum statistic bcs it is data dependent.
//
// How distribution of detection time changes?
//
// To calculate distribution of the run length - time until first detection event. 
//
// Generate signal with 1 change N times.
//
// Signal parameters: mu0, mu1, sigma, step_change_value
//
// For each signal: 
//   
//   - Save signal - maybe just (mu0, mu1, step_change_value)
//
//   Variate detector's sensitivity and 
//   
//   - ?Save output statistic
//   - Save detections 
//   
//     Variate ROI width around change location and  
//   
//       - Save start and stop positions of ROI
//   
//       Variate IN and OUT ROI detector's sensitivities and
//         
//         - Save sensitivity values (IN and OUT)
//         - ?Save output statistic
//         - Save detections when using ROI 
//
// Hyper-parameters: signal's properties: mu and sigma before and after change.
//                   value of the change (mu - mu_0)
// 
// Save simulation data and 
// Alalyze: detection delays
// Analize: probability of FA
//
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <limits>
#include <numeric>
#include <ctime>
#include <cmath>
#include <map>
#include <random>
#include <stdio.h>
#include <type_traits>
#include <assert.h>
#define SIG_LEN 101
#define CHP_LOC 51
#define N_ROLLS 50000
#define MU0 0.0
#define MU1 1.0
#define THRESHOLD_CUSUM 10.0
#define THRESHOLD_RANDOM_WALK 10.0


struct SimulationObject {
    
    const int ROI_WIDTHS_TO_CHECK [2] = {100, 110};
    float signal[SIG_LEN] {0};
    float outputStatistic[SIG_LEN] {0};
    int histogram[SIG_LEN] {0};
    std::vector<int> detections;

    void run()
    {
        simulationRandomWalk();
        simulationCusum();
    };


    void simulationCusum(){
        for (int i = 0; i < N_ROLLS; i++)
        {
            generateSignal();
            calculateCusum(MU0);
            detectChanges(THRESHOLD_CUSUM);
            calculateHistogram();
        }
        detections.clear();
        writeVec("out/stat_cusum.dat", outputStatistic);
        writeVec("out/sig.dat", signal);
        writeVec("out/hist_cusum.dat", histogram);
    };


    void simulationRandomWalk()
    {
        for (int i = 0; i < N_ROLLS; i++)
        {
            calculateRandomWalk();
            detectChanges(THRESHOLD_RANDOM_WALK);
            calculateHistogram();
        }
        detections.clear();
        writeVec("out/stat_rw.dat", outputStatistic);
        writeVec("out/hist_rw.dat", histogram);
    };


    void generateSignal()
    {
        std::random_device rd{};
        std::mt19937 gen{rd()};
        std::normal_distribution<> d{0.0, 1.0};
        for(int i = 0; i < SIG_LEN; i++)
        {
            if (i<CHP_LOC)
                signal[i] = d(gen) + MU0;
            else
                signal[i] = d(gen) + MU1;
        }
    };


    void calculateRandomWalk()
    {
        std::random_device rd{};
        std::mt19937 gen{rd()};
        std::normal_distribution<> d{0.0, 1.0};
        for(int i=0; i < SIG_LEN; i++)
        {
            if(i==0)
                outputStatistic[i] = d(gen) - MU0;
            else if(i < CHP_LOC)
                outputStatistic[i] = outputStatistic[i-1] + d(gen) - MU0;
            else
                outputStatistic[i] = outputStatistic[i-1] + d(gen) + MU1 - MU0;
        }
    };

    // plot(cumsum(rnorm(10000)-0.0 == mu0))
    // plot(cumsum(rnorm(10000)-0.1))
    
    void calculateCusum(float mu0)
    {
        for(int i = 0; i < SIG_LEN; i++)
        {
            if (i==0) 
                outputStatistic[i] = signal[i] - mu0;
            else 
                outputStatistic[i] = outputStatistic[i-1] + signal[i] - mu0;
        }
    };

    void calculateCusumUnknownMu0()
    {
        float n_r = 0;
        float mu_r = 0.0;
        for(int i = 0; i < SIG_LEN; i++)
        {
            n_r++;
            mu_r = ((n_r-1)/n_r) * mu_r + signal[i]/n_r;
            if (i==0) 
                outputStatistic[i] = signal[i] - mu_r;
            else 
                outputStatistic[i] = outputStatistic[i-1] + signal[i] - mu_r;
        }
    };


    void detectChanges(float threshold)
    {
        bool change = false;
        int t = 0;
        for(int i=0; i < SIG_LEN; i++)
        {
            if(std::abs(outputStatistic[i]) > threshold)
            {
                change = true;
                t = i;
                break;
            }
        }
        if(change)
            detections.push_back(t);
    };


    void calculateHistogram()
    {
        int tot = 0;
        for(int i = 0; i < detections.size(); i ++)
        {
            histogram[detections[i]] ++;
            tot ++;
        }
        
    };


    void writeVec(const char* fout, float vec[])
    {
        char data[100];
        FILE * pFile;
        pFile = fopen(fout, "w");
        for(int i=0; i < SIG_LEN; i++)
            fprintf(pFile, "%5.2f\n", vec[i]);
        fclose (pFile);
        std::cout << "Save output statistic into " << fout << std::endl;
        if(false)
        {
            std::ifstream infile;
            infile.open(fout);
            infile >> data;
            std::cout <<  "Read from file test " << std::endl;;
            std::cout << data << std::endl;;
            infile.close();
        }
    };

    void writeVec(const char* fout, int vec[])
    {
        FILE * pFile;
        pFile = fopen(fout, "w");
        for(int i = 0; i < SIG_LEN; i++)
            fprintf(pFile, "%3d\n", vec[i]);
        fclose (pFile);
        std::cout << "Save histogram into " << fout << std::endl;
    };


    void writeVec(const char* fout, std::vector<int> vec)
    {
        FILE * pFile;
        pFile = fopen(fout, "w");
        for(int i=0; i< vec.size(); i++)
            fprintf(pFile, "%3d\n", vec[i]);
        fclose (pFile);
        std::cout << "Save detections into " << fout << std::endl;
        std::cout << "L = " << vec.size() << std::endl;
    };


    void testRunMean(){
        float testArr[] = {10,20,30};
        float r = runningMeanTest(testArr, 3);
        std::cout << r << std::endl;
    };

    float runningMeanTest(float *arr, int n){
        float n_r = 0.0;
        float mu_r = 0.0;
        for(int i=0; i<n; i++)
        {
            n_r++;
            mu_r = ((n_r-1)/n_r) * mu_r + *(arr+i)/n_r;
            std::cout << mu_r << std::endl;
        }
        return mu_r;
    };


    void dev_calculateCusum(float lambda){
        float delta = 0.0;
        float alpha = 1.0; // HOW FAST STATISTIC WILL GROW
        float s_plus = 0.0;
        float n_r = 0;
        float mu_r = 0.0;
        for(int i = 0; i < SIG_LEN; i++)
        {
            n_r++;
            mu_r = ((n_r-1)/n_r) * mu_r + signal[i]/n_r;
            
            // old - should be equivalent
             s_plus  = s_plus * alpha + (signal[i] - mu_r - delta);
             outputStatistic[i] = s_plus;
             //assert(outputStatistic[i] == s_plus);
            // paremeterized ver: outputStatistic[i] = outputStatistic[i-1]*alpha + (signal[i] - mu_r - delta);

            // without temp var
            if (i==0)
                outputStatistic[i] = signal[i] - mu_r;
            else
                outputStatistic[i] = outputStatistic[i-1] + signal[i] - mu_r; 
        }
    };
};


int main() {
    SimulationObject m;
    m.run();
}


//for (int i = 0; i < N_ROLLS; i++)
//{
    //generateSignal();
    //if(false)
    //{
        //calculateRandomWalk();
        //detectChanges(3.5);
    //}
    //else
    //{
        //calculateCusum(1.0);
        //detectChanges(10.0);
    //}
    //calculateHistogram();
//}
//detections.clear();
//writeVec("stat.dat", outputStatistic);
//writeVec("sig.dat", signal);
//writeVec("hist.dat", histogram);

//for(int i = 0; i++ ; i < SIG_LEN)
//{
    //if(histogram[i] != 0)
        //histogram[i] = histogram[i] / tot;
//}
