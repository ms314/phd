# Plots reference

- https://github.com/naveenv92/python-science-tutorial
- https://matplotlib.org/faq/howto_faq.html#how-to
- https://matplotlib.org/tutorials/introductory/customizing.html
- [matplotlib.rc](https://matplotlib.org/api/matplotlib_configuration_api.html#matplotlib.rc)

## Settings

```py
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='x-small')
plt.rc('ytick', labelsize='x-small')
```

## Basic template for the plot

```py
fig = plt.figure(122, figsize=(15, 5))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
ax1.plot(result_rw, color="black")
ax1.set_title("Random Walk with drift")
ax1.set_ylim([np.min(result_rw), 100])
ax1.set_xlabel('T')
ax1.set_ylabel('value')
ax2.plot(result_cusum, color="black")
ax2.set_ylim([np.min(result_cusum), 100])
ax2.set_title("CUSUM")
plt.show()
```