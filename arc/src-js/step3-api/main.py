import requests
import flask
import json
import numpy as np 

sig = list(np.random.randn(100))
data = {"signal": json.dumps(sig)}
x = requests.post("http://127.0.0.1:3000/sig", json = data)

 
def cusum(signal, url_optim = "http://127.0.0.1:3000/optim"):
    data = {"signal": signal}
    r = requests.post(url_optim, json = data)
