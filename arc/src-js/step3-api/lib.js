function randomGaussianLocal() {
  // https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
  var u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

let oneChangeDetector = {
  cusumStat(sign, mu0){
    let stat = Array(sign.length);
    for(let i = 0; i < sign.length; i ++){
      if (i==0) stat[i] = sign[i] - mu0;
      else stat[i] = stat[i-1] + sign[i] - mu0;
    }
    return stat;
  },
  
  detectionFromCusumStat(outputStatistic, threshold, regionOfInterest){
    let cde = -1;
    if(regionOfInterest!==undefined){
      for(let i=regionOfInterest.left; i < regionOfInterest.right; i++){
        if(outputStatistic[i] >= threshold) {cde = i; break;}
      }
      return cde;
    }
    else{
      for(let i=0; i < outputStatistic.length; i++){
        if(outputStatistic[i] >= threshold) {cde = i; break;}
      }
      return cde;
    }
  }
}

function cusumAdaptiveMu(signal, threshold = 1.0){
  let stat = Array(signal.length).fill(0);
  let mu = 0.0;
  let change = false;
  let changes = [];
  for (let i = 0, k = 1; i < signal.length; i++,k++) 
  {
      mu = ((k-1)/k)*mu + signal[i]/k;
      if (i==0) 
        stat[i] = signal[i] - mu;
      else 
        stat[i] = stat[i-1] + signal[i] - mu;
      if(Math.abs(stat[i]) > threshold) 
        change = true;
      if(change){
        mu = signal[i];
        k = 1;
        stat[i] = 0.0;
        changes.push(i);
        change = false;
      }
  }
  return {changes: changes, stat: stat};
}

function cusumAdaptiveMuPccf(signal, trueChanges = [51, 122], threshold = 1.0){
  // detection if (i >= 51-BEFORE && i <= 51 + AFTER)
  // detection if (i >= 122-BEFORE && i <= 122 + AFTER)
  // no detection outside
  //
  // how to check if i is inside of any interval?
  let BEFORE = 5;
  let AFTER = 10;
  let stat = Array(signal.length).fill(0);
  let mu = 0.0;
  let change = false;
  let changes = [];
  //let nChanges = trueChanges.length;
  let nextChangeIndex = 0;
  for (let i = 0, k = 1; i < signal.length; i++,k++) {
    if(i==trueChanges[nextChangeIndex] + AFTER + 1) 
      nextChangeIndex += 1;
    mu = ((k-1)/k)*mu + signal[i]/k;
    if (i==0) 
      stat[i] = signal[i] - mu;
    else 
      stat[i] = stat[i-1] + signal[i] - mu;
    if(i >= trueChanges[nextChangeIndex] - BEFORE && i <= trueChanges[nextChangeIndex] + AFTER)
    {
      if(Math.abs(stat[i]) > threshold)
        change = true;
      if(change){
        mu = signal[i];
        k = 1;
        stat[i] = 0.0;
        changes.push(i);
        change = false;
      }
    }
  }
  return {changes: changes, stat: stat};
}


let perfMetrics = {
  
  closestLeftChange(changes, detection){
      let l = 0, r = changes.length - 1;
      let ans = -1;
      while(l <= r){
          let mid = Math.floor(l + (r-l)/2);
          if(changes[mid] <= detection)
          {
              ans = changes[mid];
              l = mid + 1;
          } else {
              r = mid - 1;
          }
      }
      return ans;
  },

  closestRightChange(changes, detection){
      let l = 0, r = changes.length - 1;
      let ans = -1;
      while(l <= r){
          let mid = Math.floor(l + (r-l)/2);
          if(changes[mid] >= detection)
          {
              ans = changes[mid];
              r = mid-1;
          } else {
              l = mid+1;
          }
      }
      return ans;
  },

  tpfpfnDelays(changes, detections){
      let n = detections.length;
      let mapChangeToCde = new Map(); // change => tp cde
      for (let i = 0 ; i < n; i++)
      {
          let cde = detections[i];
          let lt = this.closestLeftChange(changes, cde);
          if(lt != -1)
              if(!mapChangeToCde.has(lt))
                  mapChangeToCde.set(lt, cde);
      }
      let tps = [...mapChangeToCde.values()];
      let fps = detections.filter((x) => { return !tps.includes(x)});
      let fns = changes.filter((x) => {return ! [...mapChangeToCde.keys()].includes(x);});
      let delays = [];
      for(let chp of mapChangeToCde.keys())
          delays.push(mapChangeToCde.get(chp) - chp);
      return {tps:tps, fps:fps, fns: fns, delays: delays};
  },

  costFunction(changes, detections){
      // we minimize it
      let binaryMetrics = perfMetrics.tpfpfnDelays(changes, detections);
      let numOfNotDetectedChanges = changes.length - binaryMetrics.tps.length;
      let sumDelays = binaryMetrics.delays.reduce((a,b) => {return a+b;}, 0);
      let alpha = 0.5;
      let cost = alpha*numOfNotDetectedChanges + (1-alpha)*(binaryMetrics.fns.length + binaryMetrics.fps.length + sumDelays);
      let numFA = binaryMetrics.fps.length;
      if (numFA === undefined){
        console.log(binaryMetrics.fps.length);
      }
      return {cost:cost, numFA: numFA, sumDelays: sumDelays};
  },

  test(){
      let changes = [10, 20, 30, 40, 50, 60, 70];
      let detections = [1, 5, 11, 15, 20, 21, 25, 39, 45, 62];
      console.log(this.costFunction([10, 20], [10, 21])); 
  }
}


let optimRunner = {
  findOptimSettings: function(detectorRef){
    let sig = this.signal;   
    let changes = this.changes;
    let step = 0.1;
    let theta = 0.1;
    let bestSoFarCost = 1e6;
    let bestTheta = -1.0;
    let vecTheta = [];
    let vecCost = [];
    let vecDelays = [];
    let vecFAs = [];

    while(theta <= 20.0){
      theta += step;
      
      let rDetector = detectorRef(sig, theta);
      let detections = rDetector.changes;

      let perfRes = perfMetrics.costFunction(changes, detections);
      let cost = perfRes.cost;

      if(cost < bestSoFarCost){
        bestSoFarCost = cost;
        bestTheta = theta;
      }

      vecTheta.push(theta);
      vecCost.push(cost);
      vecDelays.push(perfRes.sumDelays);
      vecFAs.push(perfRes.numFA);
    }

    console.log("Optimal settings", bestSoFarCost, bestTheta);
    return {bestTheta:bestTheta, vecCost: vecCost, vecTheta: vecTheta, vecDelays: vecDelays, vecFAs: vecFAs};
    
  },

  runDetector: function(detectorRef, threshold){
    let sig = this.signal;
    let r = detectorRef(sig, threshold);
    return r.changes;
  }
}

let createCusumPccfRef = function(predictedChanges){return (sig, theta) => cusumMultiAdaptiveMuPccf(sig, predictedChanges, theta);};

module.exports = {
  randomGaussianLocal, 
  oneChpDetector,
  optimRunner
};