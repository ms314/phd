/// <reference path="p5.js" />
/// <reference path="p5.min.js" />

const signalSettings1 = {numPts: 501, minY: -100.0, maxY: 200.0, mu0: 0.0, sigma:1.1};
const signalSettings2 = {numPts: 501, minY: -5.0, maxY: 5.0, mu0: 0.0, sigma:1.1};
const rectangle2 = {x: 1, y: 150, w: 400, h: 100};

function randomGaussianL() {
  // https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
  var u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

let running = true;
// function generateSignal(signal){
//   return Array(signal.numPts).fill(0).map((e, i) => i < signal.numPts/2 ? randomGaussian(signal.mu0, signal.sigma): randomGaussian(1.0, signal.sigma));
// }

function mapCoordinates(rectangle, sigLen, minY, maxY, i, sigVal){
  let y = Math.max(sigVal, minY);
  y = Math.min(y, maxY);
  let xNew = i * (rectangle.w/sigLen) + rectangle.x;
  let yNew = rectangle.y + rectangle.h - (y-minY) * (rectangle.h/(maxY-minY));
  return [xNew, yNew];
}


function cusum(sign, mu0){
  let stat = Array(sign.length);
  for(let i = 0; i < sign.length; i ++){
    if (i==0) stat[i] = sign[i] - mu0;
    else stat[i] = stat[i-1] + sign[i] - mu0;
  }
  return stat;
}

function detector(outputStatistic, threshold, regionOfInterest){
  let cde = -1;
  if(regionOfInterest!==undefined){
    for(let i=regionOfInterest.left; i < regionOfInterest.right; i++){
      if(outputStatistic[i] >= threshold) {cde = i; break;}
    }
    return cde;
  }
  else{
    for(let i=0; i < outputStatistic.length; i++){
      if(outputStatistic[i] >= threshold) {cde = i; break;}
    }
    return cde;
  }
  
}

function getSimulationResult() {
  //const signalSettings1 = {numPts: 501, minY: -100.0, maxY: 200.0, mu0: 0.0, sigma:1.1};
  let signalSettings = {numPts: 501, changePoint: 251, mu0: 0.0, mu1: 2.0, sigma:1.1};
  let roi = {left: 230, right: 300};
  
  let signal = Array(signalSettings.numPts).fill(0)
  for(let i=0; i<signal.length; i++){
    if(i<= signalSettings.changePoint)
      signal[i] = randomGaussianL()* signalSettings.sigma+signalSettings.mu0;
    else 
      signal[i] = randomGaussianL()* signalSettings.sigma+signalSettings.mu1;
  }

  let cusumStat = cusum(signal, signalSettings.mu0);

  let thCommon = 20;
  let thresholdStatic = thCommon;
  let thresholdDynamic = thCommon;

  let cdeStat = detector(cusumStat, thresholdStatic);
  let cdeDyn = detector(cusumStat, thresholdDynamic, roi);


  if(cdeStat >= signalSettings.changePoint)
    console.log("Detection delay (static ):", cdeStat - signalSettings.changePoint);
  else 
    console.log("FA (stat):", cdeStat - signalSettings.changePoint);
  
  if(cdeDyn >= signalSettings.changePoint)
    console.log("Detection delay (dynamic):", cdeDyn - signalSettings.changePoint);
  else 
    console.log("FA (dynamic):", cdeDyn - signalSettings.changePoint);
  
  console.log('  ');

  if(cdeDyn >= signalSettings.changePoint && cdeStat < signalSettings.changePoint){
    //noLoop();
    running = false;
    //textSize(60);
    //fill(255,255,0);
    //text("Detection delay is reduced, FA is skipped!!!", 0, height + 10);
    // p1 = createP("")
    // p2 = createP("")
    // p3 = createP("")
    // p4 = createP("")
    // p5 = createP("")
    // p6 = createP("")
    // p1.position(0,300);
    // p2.position(0,320);
    // p3.position(0,340);
    // p4.position(0,360);
    // p5.position(0,390);
    // p6.position(0,410);


    if(typeof p1 != "undefined") p1.remove();
    if(typeof p2 != "undefined") p2.remove();
    if(typeof p3 != "undefined") p3.remove();
    if(typeof p4 != "undefined") p4.remove();

    //if(typeof p5 != "undefined") p5.remove();
    //if(typeof p6 != "undefined") p6.remove();

    p1 = createP("!! Detection delay is reduced, FA is skipped!!!");
    p2 = createP("Changepoint........... : " + signalSettings.changePoint);
    p3 = createP("Static detection...... : " + cdeStat + "; DELAY = " + (cdeStat - signalSettings.changePoint));
    p4 = createP("Dynamic detection. : "+ cdeDyn + "; DELAY = " + (cdeDyn - signalSettings.changePoint));
    p5 = createP("* Click space or click mouse to continue.")
    p6 = createP("* Top plot - CuSum output statistic; Bottom - corresponding signal; Red - detections and threshold; Blue - ROI")

    p1.position(0,300);
    p2.position(0,320);
    p3.position(0,340);
    p4.position(0,360);
    p5.position(0,390);
    p6.position(0,410);

  }



  return {signal: signal, 
          cusumStat: cusumStat, 
          roi: roi, 
          thresholdDynamic: thresholdDynamic, 
          thresholdStatic: thresholdStatic,
          cdeStat: cdeStat,
          cdeDyn: cdeDyn,
          signalSettings:signalSettings,
        };
};

function setup() {
  createCanvas(400, 300);
  frameRate(10);
}


function drawSimulation(){
  if(!running) return;
  background(255,255,255);
  let r = getSimulationResult();
  let sig = r.signal;//  generateSignal(signalSettings1);
  let cusumStat = r.cusumStat;// cusum(sig, signalSettings1.mu0);
  
  function plotData(recProp, sigProp, dataToPlot){
    let mxY = max(dataToPlot);
    let mnY = min(dataToPlot);
    let n = dataToPlot.length;
    let [px, py] = mapCoordinates(recProp, n, mnY, mxY, 0, dataToPlot[0]);
    let x, y;
    for(let i = 0; i < n; i++){
      [x,y] = mapCoordinates(recProp, n, mnY, mxY, i, dataToPlot[i]);
      line(px, py, x, y);
      px = x;
      py = y;
    }
  }
  const rectangle1 = {x: 1, y: 1, w: 400, h: 100};
  plotData(rectangle1, r.signalSettings, cusumStat);
  plotData(rectangle2, r.signalSettings, sig);

  // V line
  let localMapperVline = function(xCoordinate){
    return mapCoordinates(rectangle1, cusumStat.length,  min(cusumStat), max(cusumStat), xCoordinate, 0);
  }
  // detection location and level
  let [xStat, ] = localMapperVline(r.cdeStat);
  let [xDyn, ] = localMapperVline(r.cdeDyn);
  line(xStat, height, xStat, 0);
  stroke('red');
  line(xDyn, height, xDyn, 0);
  stroke('black');
  // H line
  let localMapperHline = function(yCoordinate){
    return mapCoordinates(rectangle1, cusumStat.length,  min(cusumStat), max(cusumStat), 0, yCoordinate);
  }
  stroke('red');
  let [,yStat] = localMapperHline(r.thresholdStatic); 
  let [,yDyn] = localMapperHline(r.thresholdDynamic);
  line(0, yStat, width, yStat);
  line(0, yDyn, width, yDyn);
  stroke('black');
  // bbox
  strokeWeight(2);
  stroke('blue');
  let [roiLeft,] = localMapperVline(r.roi.left);
  let [roiRight,] = localMapperVline(r.roi.right);
  line(roiLeft, 0, roiLeft, height);
  line(roiRight, 0, roiRight, height);
  stroke('black');
  strokeWeight(1);
}

function mouseClicked(){
  if(!running) running=! running;
}

function keyPressed(){
  if(keyCode == 32){
    if(!running) running=! running;
  }
  //console.log(keyCode);
}

function mousePressed() {
  //noLoop();
  
  //loop();
}

function mouseReleased() {
  // loop();
  //if(!running) running=!running;
}

function draw() {
//  SimulationmStat();
 drawSimulation();
}