//import {randomGaussianL} from 'utils.js';
const utils = require('./utils.js');
if(1==0) console.log(utils.random());

function runningMu(signal){
    let mu = 0.0;
    for (let i = 0, k = 1; i < signal.length; i++, k++)
    {
        mu = ((k-1)/k)*mu + signal[i]/k;
    }
    return mu;
}

let changes = [51];
let width = 3;
let nextChpIdx = 0;
for(let i = 0; i < 251; i ++){
    if(i == changes[nextChpIdx] + width+1) nextChpIdx += 1;
    if ( i >= changes[nextChpIdx] - width && i <= changes[nextChpIdx] + width){
        //console.log(i + " is within PI");
    }
}


function makeAdder(x) {
    return function(y) {
      return x + y;
    };
}
var add5 = makeAdder(5);
var add10 = makeAdder(10);
console.log(add5(2));  // 7
console.log(add10(2)); // 12

let f = function(x,y){return x * y + 1;} 
let ref2 = function(x,y){return f(x,y);}
console.log(ref2(9, 9));

let f3 = function(a,b,c){return a+b+c;}
let f2 = function(c){return (a,b) => f3(a,b,c);}
console.log(f2(1)(1,1));

function tpfpfnDelays(changes, detections){
    let n = detections.length;
    let mapChangeToCde = new Map(); // change => tp cde
    for (let i = 0 ; i < n; i++){
        let cde = detections[i];
        let lt = this.closestLeftChange(changes, cde);
        if(lt != -1){
            if(!mapChangeToCde.has(lt)){
                mapChangeToCde.set(lt, cde);
            }
        }
    }
    let tps = [...mapChangeToCde.values()];
    let fps = detections.filter((x) => { return !tps.includes(x)});
    let fns = changes.filter((x) => {return ! [...mapChangeToCde.keys()].includes(x);});
    let delays = [];
    for(let chp of mapChangeToCde.keys()){
        delays.push(mapChangeToCde.get(chp) - chp);
    }
    return {tps:tps, fps:fps, fns: fns, delays: delays};
}

let r = tpfpfnDelays([10, 20], [])
console.log(r);