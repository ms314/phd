function randomGaussian() {
    // https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

exports.random = randomGaussian;


// function generateSignal(signal){
//   return Array(signal.numPts).fill(0).map((e, i) => i < signal.numPts/2 ? randomGaussian(signal.mu0, signal.sigma): randomGaussian(1.0, signal.sigma));
// }
