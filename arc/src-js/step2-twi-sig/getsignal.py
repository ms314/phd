import json

path = "/Users/al314/gitlocal/sklz/phd/phd-monitor-2019-bck-2020-05/datasets/exchange-rate-bank-of-twi/data/sigTwi.dat"

with open(path, 'r') as f:
    cont = f.readlines()
cont = list(map(lambda x: float(x), cont))

with open("./dat/sigtwi.json", "w") as f:
    json.dump({"sigTwi": cont}, f)
