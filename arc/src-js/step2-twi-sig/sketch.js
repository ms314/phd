/// <reference path="p5.js" />
/// <reference path="p5.min.js" />

const signalSettings1 = {numPts: 501, minY: -100.0, maxY: 200.0, mu0: 0.0, sigma:1.1};
const signalSettings2 = {numPts: 501, minY: -5.0, maxY: 5.0, mu0: 0.0, sigma:1.1};
const rectangle1 = {x: 1, y: 1  , w: 1200, h: 100};
const rectangle2 = {x: 1, y: 150, w: 300, h: 100};
const rectangle3 = {x: 1, y: 250, w: 300, h: 100};
const rectangle4 = {x: 1, y: 370, w: 300, h: 100};

let running = true;

function randomGaussianLocal() {
  // https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
  var u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

function mapCoordinates(rectangle, sigLen, minY, maxY, i, sigVal){
  let y = Math.max(sigVal, minY);
  y = Math.min(y, maxY);
  let xNew = i * (rectangle.w/sigLen) + rectangle.x;
  let yNew = rectangle.y + rectangle.h - (y-minY) * (rectangle.h/(maxY-minY));
  return [xNew, yNew];
}

function cusum(sign, mu0){
  let stat = Array(sign.length);
  for(let i = 0; i < sign.length; i ++){
    if (i==0) stat[i] = sign[i] - mu0;
    else stat[i] = stat[i-1] + sign[i] - mu0;
  }
  return stat;
}

function detector(outputStatistic, threshold, regionOfInterest){
  let cde = -1;
  if(regionOfInterest!==undefined){
    for(let i=regionOfInterest.left; i < regionOfInterest.right; i++){
      if(outputStatistic[i] >= threshold) {cde = i; break;}
    }
    return cde;
  }
  else{
    for(let i=0; i < outputStatistic.length; i++){
      if(outputStatistic[i] >= threshold) {cde = i; break;}
    }
    return cde;
  }
}

let testSignal = function(){
  let n = 50;
  let a = Array(n).fill(0).map(() => {return randomGaussianLocal();});
  let b = Array(n).fill(0).map(() => {return randomGaussianLocal() + 2.0;});
  return a.concat(b);
}

function cusumMultiAdaptiveMu(signal, threshold = 1.0){
  let stat = Array(signal.length).fill(0);
  let mu = 0.0;
  let change = false;
  let changes = [];
  for (let i = 0, k = 1; i < signal.length; i++,k++) {
      mu = ((k-1)/k)*mu + signal[i]/k;
      if (i==0) {
        stat[i] = signal[i] - mu;
      } else {
        stat[i] = stat[i-1] + signal[i] - mu;
      }
      if(Math.abs(stat[i]) > threshold){
        change = true;
      }
      if(change){
        mu = signal[i];
        k = 1;
        stat[i] = 0.0;
        changes.push(i);
        change = false;
        //console.log("detection = " + i);
      }
  }
  return {changes: changes, stat: stat};
}
//cusumMultiAdaptiveMu(testSignal(), 5);


function cusumMultiAdaptiveMuPccf(signal, trueChanges = [51, 122], threshold = 1.0){
  // detection if (i >= 51-BEFORE && i <= 51 + AFTER)
  // detection if (i >= 122-BEFORE && i <= 122 + AFTER)
  // no detection outside
  //
  // how to check if i is inside of any interval?
  let BEFORE = 5;
  let AFTER = 10;
  let stat = Array(signal.length).fill(0);
  let mu = 0.0;
  let change = false;
  let changes = [];

  let nChanges = trueChanges.length;
  let nextChangeIndex = 0;

  for (let i = 0, k = 1; i < signal.length; i++,k++) {

    if(i==trueChanges[nextChangeIndex] + AFTER + 1) nextChangeIndex += 1;

    mu = ((k-1)/k)*mu + signal[i]/k;

    if (i==0) {
      stat[i] = signal[i] - mu;
    } else {
      stat[i] = stat[i-1] + signal[i] - mu;
    }

    if(i >= trueChanges[nextChangeIndex] - BEFORE && i <= trueChanges[nextChangeIndex] + AFTER){
      if(Math.abs(stat[i]) > threshold){
        change = true;
      }
      if(change){
        mu = signal[i];
        k = 1;
        stat[i] = 0.0;
        changes.push(i);
        change = false;
      }
    }
  }
  return {changes: changes, stat: stat};
}


let createCusumPccfRef = function(predictedChanges){return (sig, theta) => cusumMultiAdaptiveMuPccf(sig, predictedChanges, theta);};

let step1 = {
  getSimulationResult: function () {
    //const signalSettings1 = {numPts: 501, minY: -100.0, maxY: 200.0, mu0: 0.0, sigma:1.1};
    let signalSettings = {numPts: 501, changePoint: 251, mu0: 0.0, mu1: 2.0, sigma:1.1};
    let roi = {left: 230, right: 300};
    
    let signal = Array(signalSettings.numPts).fill(0)
    for(let i=0; i<signal.length; i++){
      if(i<= signalSettings.changePoint)
        signal[i] = randomGaussianLocal()* signalSettings.sigma+signalSettings.mu0;
      else 
        signal[i] = randomGaussianLocal()* signalSettings.sigma+signalSettings.mu1;
    }
    
    let cusumStat = cusum(signal, signalSettings.mu0);
    
    let thCommon = 20;
    let thresholdStatic = thCommon;
    let thresholdDynamic = thCommon;
    
    let cdeStat = detector(cusumStat, thresholdStatic);
    let cdeDyn = detector(cusumStat, thresholdDynamic, roi);
    
    
    if(cdeStat >= signalSettings.changePoint)
    console.log("Detection delay (static ):", cdeStat - signalSettings.changePoint);
    else 
    console.log("FA (stat):", cdeStat - signalSettings.changePoint);
    
    if(cdeDyn >= signalSettings.changePoint)
    console.log("Detection delay (dynamic):", cdeDyn - signalSettings.changePoint);
    else 
    console.log("FA (dynamic):", cdeDyn - signalSettings.changePoint);
    
    console.log('  ');
    
    if(cdeDyn >= signalSettings.changePoint && cdeStat < signalSettings.changePoint){
      running = false;
      
      
      if(typeof p1 != "undefined") p1.remove();
      if(typeof p2 != "undefined") p2.remove();
      if(typeof p3 != "undefined") p3.remove();
      if(typeof p4 != "undefined") p4.remove();
      
      p1 = createP("!! Detection delay is reduced, FA is skipped!!!");
      p2 = createP("Changepoint........... : " + signalSettings.changePoint);
      p3 = createP("Static detection...... : " + cdeStat + "; DELAY = " + (cdeStat - signalSettings.changePoint));
      p4 = createP("Dynamic detection. : "+ cdeDyn + "; DELAY = " + (cdeDyn - signalSettings.changePoint));
      p5 = createP("* Click space or click mouse to continue.")
      p6 = createP("* Top plot - CuSum output statistic; Bottom - corresponding signal; Red - detections and threshold; Blue - ROI")
      
      p1.position(0,300);
      p2.position(0,320);
      p3.position(0,340);
      p4.position(0,360);
      p5.position(0,390);
      p6.position(0,410);
    }
    
    return {signal: signal, 
      cusumStat: cusumStat, 
      roi: roi, 
      thresholdDynamic: thresholdDynamic, 
      thresholdStatic: thresholdStatic,
      cdeStat: cdeStat,
      cdeDyn: cdeDyn,
      signalSettings:signalSettings,
    };
  },
  
  drawSimulationResults: function (){
    if(!running) return;
    background(255,255,255);
    let r = this.getSimulationResult();
    let sig = r.signal;//  generateSignal(signalSettings1);
    let cusumStat = r.cusumStat;// cusum(sig, signalSettings1.mu0);
    
    function plotData(recProp, sigProp, dataToPlot){
      let mxY = max(dataToPlot);
      let mnY = min(dataToPlot);
      let n = dataToPlot.length;
      let [px, py] = mapCoordinates(recProp, n, mnY, mxY, 0, dataToPlot[0]);
      let x, y;
      for(let i = 0; i < n; i++){
        [x,y] = mapCoordinates(recProp, n, mnY, mxY, i, dataToPlot[i]);
        line(px, py, x, y);
        px = x;
        py = y;
      }
    }
    
    const rectangle1 = {x: 1, y: 1, w: 400, h: 100};
    plotData(rectangle1, r.signalSettings, cusumStat);
    plotData(rectangle2, r.signalSettings, sig);
    
    // V line
    let localMapperVline = function(xCoordinate){
      return mapCoordinates(rectangle1, cusumStat.length,  min(cusumStat), max(cusumStat), xCoordinate, 0);
    }
    // detection location and level
    let [xStat, ] = localMapperVline(r.cdeStat);
    let [xDyn, ] = localMapperVline(r.cdeDyn);
    line(xStat, height, xStat, 0);
    stroke('red');
    line(xDyn, height, xDyn, 0);
    stroke('black');
    // H line
    let localMapperHline = function(yCoordinate){
      return mapCoordinates(rectangle1, cusumStat.length,  min(cusumStat), max(cusumStat), 0, yCoordinate);
    }
    stroke('red');
    let [,yStat] = localMapperHline(r.thresholdStatic); 
    let [,yDyn] = localMapperHline(r.thresholdDynamic);
    line(0, yStat, width, yStat);
    line(0, yDyn, width, yDyn);
    stroke('black');
    // bbox
    strokeWeight(2);
    stroke('blue');
    let [roiLeft,] = localMapperVline(r.roi.left);
    let [roiRight,] = localMapperVline(r.roi.right);
    line(roiLeft, 0, roiLeft, height);
    line(roiRight, 0, roiRight, height);
    stroke('black');
    strokeWeight(1);
  }
}


let perfMetrics = {
  closestLeftChange(changes, detection){
      let l = 0, r = changes.length - 1;
      let ans = -1;
      while(l <= r){
          let mid = Math.floor(l + (r-l)/2);
          if(changes[mid] <= detection){
              ans = changes[mid];
              l = mid + 1;
          } else {
              r = mid - 1;
          }
      }
      return ans;
  },

  closestRightChange(changes, detection){
      let l = 0, r = changes.length - 1;
      let ans = -1;
      while(l <= r){
          let mid = Math.floor(l + (r-l)/2);
          if(changes[mid] >= detection){
              ans = changes[mid];
              r = mid-1;
          } else {
              l = mid+1;
          }
      }
      return ans;
  },

  tpfpfnDelays(changes, detections){
      let n = detections.length;
      let mapChangeToCde = new Map(); // change => tp cde
      for (let i = 0 ; i < n; i++){
          let cde = detections[i];
          let lt = this.closestLeftChange(changes, cde);
          if(lt != -1){
              if(!mapChangeToCde.has(lt)){
                  mapChangeToCde.set(lt, cde);
              }
          }
      }
      let tps = [...mapChangeToCde.values()];
      let fps = detections.filter((x) => { return !tps.includes(x)});
      let fns = changes.filter((x) => {return ! [...mapChangeToCde.keys()].includes(x);});
      let delays = [];
      for(let chp of mapChangeToCde.keys()){
          delays.push(mapChangeToCde.get(chp) - chp);
      }
      return {tps:tps, fps:fps, fns: fns, delays: delays};
  },

  costFunction(changes, detections){
      // we minimize it
      let binaryMetrics = perfMetrics.tpfpfnDelays(changes, detections);
      let numOfNotDetectedChanges = changes.length - binaryMetrics.tps.length;
      let sumDelays = binaryMetrics.delays.reduce((a,b) => {return a+b;}, 0);
      let alpha = 0.5;
      //let cost = numOfNotDetectedChanges + binaryMetrics.fns.length + binaryMetrics.fps.length + sumDelays;
      let cost = alpha*numOfNotDetectedChanges + (1-alpha)*(binaryMetrics.fns.length + binaryMetrics.fps.length + sumDelays);
      let numFA = binaryMetrics.fps.length;
      if (numFA === undefined){
        console.log(binaryMetrics.fps.length);
      }
      //console.log(numFA);
      return {cost:cost, numFA: numFA, sumDelays: sumDelays};
  },

  test(){
      let changes = [10, 20, 30, 40, 50, 60, 70];
      let detections = [1, 5, 11, 15, 20, 21, 25, 39, 45, 62];
      // let tps = [11, 20, 39, 45, 62];
      // let fps = [1, 5, 15, 21, 25]
      // let fns = [50, 70]
      console.log(this.costFunction([10, 20], [10, 21])); 
  }
}


let step2 = {

  signal: [100.0, 99.6, 99.4, 99.1, 99.2, 99.2, 99.2, 99.3, 100.0, 100.0, 100.0, 100.0, 99.8, 99.8, 99.7, 99.3, 99.8, 99.4, 99.3, 99.8, 98.8, 98.1, 97.8, 97.9, 97.9, 98.5, 98.6, 98.5, 98.9, 99.8, 99.4, 106.5, 105.9, 110.7, 110.5, 110.5, 109.1, 107.9, 107.9, 108.9, 113.9, 113.8, 117.3, 118.1, 121.3, 119.0, 115.5, 116.0, 117.0, 117.3, 118.8, 120.5, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 105.3, 86.9, 92.2, 92.2, 92.5, 92.5, 92.5, 92.5, 92.5, 92.5, 91.5, 91.5, 90.9, 90.0, 89.4, 89.2, 88.5, 87.5, 87.5, 87.3, 86.2, 84.7, 83.7, 83.6, 82.2, 83.2, 82.7, 82.8, 82.4, 82.3, 82.8, 83.1, 83.1, 83.3, 83.5, 83.5, 83.7, 83.6, 83.3, 83.2, 83.5, 84.4, 84.5, 84.7, 85.0, 85.5, 85.8, 85.8, 86.2, 86.8, 87.1, 87.5, 88.2, 88.8, 89.5, 90.8, 92.9, 94.2, 94.4, 93.8, 93.0, 91.2, 90.5, 90.1, 89.2, 88.8, 88.2, 88.0, 88.2, 86.1, 84.1, 83.8, 83.9, 83.5, 83.4, 83.6, 82.4, 76.1, 76.2, 77.6, 77.7, 78.8, 79.6, 80.4, 81.7, 82.0, 81.1, 83.5, 84.2, 82.9, 82.6, 81.6, 79.2, 78.7, 80.1, 80.3, 82.0, 83.5, 81.3, 80.8, 72.3, 69.2, 64.2, 64.8, 65.0, 68.5, 66.4, 64.8, 63.4, 60.9, 60.7, 62.7, 59.8, 61.1, 61.4, 60.7, 56.3, 49.3, 50.3, 51.9, 54.0, 54.3, 55.0, 52.8, 54.1, 55.4, 54.5, 55.8, 56.6, 55.3, 55.2, 56.2, 51.7, 52.3, 52.0, 52.5, 53.1, 53.8, 55.2, 58.9, 59.8, 61.0, 61.8, 60.0, 61.1, 63.7, 63.2, 66.7, 59.6, 62.2, 60.0, 58.8, 59.4, 57.8, 60.0, 59.8, 60.7, 60.4, 61.1, 59.4, 59.0, 59.8, 59.6, 60.2, 61.6, 60.4, 61.4, 61.6, 57.2, 56.9, 57.3, 57.5, 57.9, 59.7, 60.2, 58.5, 59.7, 60.1, 60.5, 60.6, 59.3, 59.0, 55.9, 55.8, 56.8, 58.6, 57.9, 56.9, 55.2, 54.7, 51.6, 51.7, 51.9, 51.8, 52.4, 51.6, 52.7, 52.9, 52.3, 49.4, 49.5, 50.6, 49.3, 47.3, 49.4, 49.1, 50.8, 54.0, 53.6, 52.1, 52.6, 54.6, 53.0, 53.9, 53.9, 53.4, 53.1, 55.5, 56.2, 54.5, 52.9, 50.7, 49.6, 48.5, 48.4, 51.0, 53.9],

  //changes: [31, 53, 79, 135, 175, 225, 280],
  //
  // next from: console.log(detections);
  changesOpt: [27, 32, 41, 51, 53, 78, 84, 93, 100, 122, 131, 137, 144, 150, 155, 161, 177, 180, 188, 194, 199, 218, 234, 249, 265, 276, 286, 301],
  //
  // let's remove several
  changes: [29, 50, 77, 133, 153, 175, 193, 223].map((x) => {return x+0;}),


  plotSignal: function(recProp, inputSignal){
    let sig;
    if(inputSignal===undefined)
      sig = this.signal;
    else
      sig = inputSignal;
    let [mn, mx, n] = [Math.min(...sig), Math.max(...sig), sig.length];
    let mapper = function(x,y){
      return mapCoordinates(recProp, n, mn, mx, x, y);
    }
    let [px,py]=mapper(0, sig[0]);
    point(px,py);
    let x,y;
    for(let i=0; i<n;i++){
      [x,y]=mapper(i, sig[i]);
      strokeWeight(2);
      point(px,py);
      strokeWeight(1);
      line(px,py,x,y);
      px=x;
      py=y;
    }
  },

  plotVerticalLines: function(recProp, chps) {
    let sig = this.signal;
    //let chps = this.changes;
    let [mn, mx, n] = [min(sig), max(sig), sig.length];
    let mapper = function(x,y){
      return mapCoordinates(recProp, n, mn, mx, x, y);
    }
    for(let k=0; k<chps.length; k++){
      let [a,] = mapper(chps[k], 0);
      line(a,0, a, recProp.h);
    }
  },

  findOptimSettings: function(detectorRef){
    let sig = this.signal;
   
    let changes = this.changes;
    let step = 0.1;
    let theta = 0.1;
    let bestSoFarCost = 1e6;
    let bestTheta = -1.0;
    let vecTheta = [];
    let vecCost = [];
    let vecDelays = [];
    let vecFAs = [];

    while(theta <= 20.0){
      theta += step;
      
      let rDetector = detectorRef(sig, theta);
      let detections = rDetector.changes;

      let perfRes = perfMetrics.costFunction(changes, detections);
      let cost = perfRes.cost;

      if(cost < bestSoFarCost){
        bestSoFarCost = cost;
        bestTheta = theta;
      }

      vecTheta.push(theta);
      vecCost.push(cost);
      vecDelays.push(perfRes.sumDelays);
      vecFAs.push(perfRes.numFA);
    }

    console.log("Optimal settings", bestSoFarCost, bestTheta);
    return {bestTheta:bestTheta, vecCost: vecCost, vecTheta: vecTheta, vecDelays: vecDelays, vecFAs: vecFAs};
    
  },

  runDetector: function(detectorRef, threshold){
    let sig = this.signal;
    let r = detectorRef(sig, threshold);
    return r.changes;
  }
}

function setup() {
  createCanvas(1200, 800);
  //background(255,255,200);
  frameRate(0.5);
  //noLoop();
}

let refCusumPccf = createCusumPccfRef(step2.changes);

function draw() {
  step2.plotSignal(rectangle1);
  background(200,200,200);  
  // True Changes
  stroke('red');
  strokeWeight(4);
  step2.plotVerticalLines(rectangle1, step2.changes);
  strokeWeight(1);
  stroke('black');
  let rOptSettings = step2.findOptimSettings(cusumMultiAdaptiveMu);
  let detections = step2.runDetector(cusumMultiAdaptiveMu, rOptSettings.bestTheta);
  
  // Detections and cost
  stroke('black');
  step2.plotVerticalLines(rectangle1, detections);
  stroke('black');
  step2.plotSignal(rectangle2, rOptSettings.vecCost);
  step2.plotSignal(rectangle3, rOptSettings.vecDelays);
  step2.plotSignal(rectangle4, rOptSettings.vecFAs);

  // Pccf detections and cost
  let rOptSettingsPccf = step2.findOptimSettings(refCusumPccf);
  let detectionsPccf = step2.runDetector(refCusumPccf, rOptSettingsPccf.bestTheta);
  stroke('blue');
  step2.plotVerticalLines(rectangle1, detectionsPccf);
  step2.plotSignal(rectangle2, rOptSettingsPccf.vecCost);
  step2.plotSignal(rectangle3, rOptSettingsPccf.vecDelays);
  step2.plotSignal(rectangle4, rOptSettingsPccf.vecFAs);
  stroke('black');
}

function mouseClicked(){
  if(!running) running=! running;
}

function keyPressed(){
  if(keyCode == 32){
    if(!running) running=! running;
  }
}
