import datetime
from suntime import Sun
import matplotlib.pyplot as plt

latitude = 62.264
longitude = 25.806
sun = Sun(latitude, longitude)
today_sr = sun.get_sunrise_time()
today_ss = sun.get_sunset_time()


def get_day_len(dt):
    sr_time = sun.get_sunrise_time(dt)
    ss_time = sun.get_sunset_time(dt)
    day_length = ss_time - sr_time
    return day_length


day_lengths = []
for i in range(60):
    date = datetime.date.today() + datetime.timedelta(days=i)
    day_len = get_day_len(date)
    day_lengths.append((date, day_len))

increases = []
for i, e in enumerate(day_lengths):
    if i == 0:
        continue
    else:
        delta = e[1] - day_lengths[i-1][1]
        if i == 1:
            acc_delta = delta
        else:
            acc_delta = delta + increases[-1][2]
        increases.append((e[0], delta.seconds/60.0, acc_delta))

for day, delta, acc in increases:
    print(f"{day.day:2d}/{day.month:2d}/{day.year} "
          f"+ {delta} min. (acc. {int(acc.seconds/60):3d} min.)")

plt.xticks(rotation=45)
plt.ylabel('Day time increase + H.')
plt.grid()
plt.plot([e[0] for e in increases],
         [e[2].seconds/60/60 for e in increases])

plt.show()