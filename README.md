# phd

## Checks for reproducibility

- Commit only if `pytest` runs
- Also, **paper plots should run:** `make_paper_plots.py`
- Check that all images are created recently `ls -al`


## Temperature data first observation

```
python -c "from datetime import datetime; print(datetime.utcfromtimestamp(1615050705.5283835).strftime('%Y-%m-%d %H:%M:%S'))"
> 2021-03-06 17:11:45
```

## Install

- `python -m venv venv`
- `source ./venv/bin/activate`
- `pip install git+https://gitlab.com/ms314/phd.git`

## Submit

### Submit (current): ECML PKDD journal

- https://2021.ecmlpkdd.org/?page_id=1341
- https://www.springer.com/journal/10618
- template: http://static.springer.com/sgw/documents/468198/application/zip/LaTeX_DL_468198_240419.zip


### Submit DSAA journal, if we will miss ECML PKDD journal

- Template:
- https://dsaa2021.dcc.fc.up.pt/calls/journal-track
- https://www.springer.com/journal/10994
- template: http://static.springer.com/sgw/documents/468198/application/zip/LaTeX_DL_468198_240419.zip


### Submit (Missed) ECML PKDD conference

- https://2021.ecmlpkdd.org/?page_id=1599
- https://www.springer.com/gp/computer-science/lncs/new-latex-templates-available/15634678



problem:
sequential detection
if no detection between change1 and change2
then return inf or empty?
if empty then average might be low even if there is no detection

### Important notes

- Pccf can be used without threshold and probability notions - just a sequence of equal distanced moments.
  Instead of threshold we can just choose number of predictions.

- FA are because of Random Walk (RW) nature of CUSUM output statistic.
  Before change it is RW, after - RW with drift.


### Tests

- run `pytest`

### Set up

- `python -m venv venv`
- `source ./venv/bin/activate`
- `pip install -r requirements.txt`
- `pip install -e .` - to access package as `pccf`
- `ppytest`
