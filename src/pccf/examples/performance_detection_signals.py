from pccf.data import *
from pccf.utils_pccf import *
from pccf.detector import make_cusum_pccf, cusum
from pccf.settings import *
from pccf.utils_general import path_ext_low_with_dot
from numpy.random import randn
from pccf.detector_performance import *
from loguru import logger
from pccf.pccf_performance import *

SAVE_PLOTS_DIR = './img'


def p(fif_file_name):
    return os.path.join(SAVE_PLOTS_DIR, fif_file_name)


def performance_signal(sig,
                       sig_changes,
                       rois,
                       theta_vec=gen_theta_vec(0.1, 1, 20),
                       save_fig=None):
    """ Prediction and detection steps """

    cost_stat, delays_stat, fps_stat, fns_stat, tps_stat, f1_stat = \
        collect_performance(cusum, sig, sig_changes, theta_vec)

    cusum_pccf_ref = make_cusum_pccf(rois)
    cost_dyn, delays_dyn, fps_dyn, fns_dyn, tps_dyn, f1_dyn = \
        collect_performance(cusum_pccf_ref, sig, sig_changes, theta_vec,
                            rois=rois)

    if 1 == 1:
        common_font_size = 30
        common_line_width = 4
        common_tick_size = 30

        fig = plt.figure(figsize=(20, 8))
        ax1 = fig.add_subplot(121)
        ax1.plot(theta_vec, delays_stat, ls="-", color="black", linewidth=common_line_width, label='Fixed settings')
        ax1.plot(theta_vec, delays_dyn,  ls="--", color="black", linewidth=common_line_width, label='Pccf')
        # ax1.set_title('Detection delays', fontsize=common_font_size)
        ax1.set_xlabel('Threshold', fontsize=common_font_size)
        ax1.set_ylabel('Detection delay', fontsize=common_font_size)
        ax1.xaxis.set_tick_params(labelsize=common_tick_size)
        ax1.yaxis.set_tick_params(labelsize=common_tick_size)
        ax1.legend(loc='upper center', shadow=False, fontsize=30,  bbox_to_anchor=(0.5, 1.2), prop={'size': 30}, ncol=2)

        ax2 = fig.add_subplot(122)
        ax2.plot(theta_vec, f1_stat, ls="-", color="black", linewidth=common_line_width)
        ax2.plot(theta_vec, f1_dyn, ls="--", color="black", linewidth=common_line_width)
        # ax2.set_title('F1', fontsize=common_font_size)
        ax2.set_xlabel('Threshold', fontsize=common_font_size)
        ax2.set_ylabel('F1', fontsize=common_font_size)
        ax2.xaxis.set_tick_params(labelsize=common_tick_size)
        ax2.yaxis.set_tick_params(labelsize=common_tick_size)
        ax2.set_ylim((0, 1.0))
        plt.tight_layout()
        plt.legend(frameon=False)

        # Put a nicer background color on the legend.
        # legend1.get_frame().set_facecolor('C0')

        logger.info(f"Best f1 pccf {f1_dyn[np.argmax(f1_dyn)]}")
        logger.info(f"Best f1 stat {f1_stat[np.argmax(f1_stat)]}")

        if save_fig:
            if path_ext_low_with_dot(save_fig) == '.eps':
                plt.savefig(save_fig, format='eps')
            elif path_ext_low_with_dot(save_fig) == '.png':
                plt.savefig(save_fig, format='png', dpi=300)
            else:
                raise NotImplementedError
            logger.info(f"Save Figure into {save_fig}")
            plt.close(fig)
        else:
            plt.show()


def get_perfect_rois(changes, radius, left_margin=None, right_margin=None):
    """
    Use only for artificial signal.
    Or for benchmarking
    """
    rois_perfect = []
    for change in changes:
        new_roi = Roi(change, radius)
        if left_margin and right_margin:
            new_roi.set_left_right(change - left_margin, change + right_margin)
        rois_perfect.append(new_roi)
    return rois_perfect


# def run_earth_quakes():
#
#     predicted_rois = pccf_rois_adhoc(changes_earth_quakes,
#                                      ROI_RADIUS_EARTH_QUAKES)
#     performance_signal(sig_earth_quakes,
#                        changes_earth_quakes,
#                        predicted_rois,
#                        save_fig=p('performance_earth_quakes.PNG'))  # '../../tex/img/performance_earth_quakes.eps'
#
#     perfect_rois = get_perfect_rois(changes_earth_quakes,
#                                     ROI_RADIUS_EARTH_QUAKES)
#     performance_signal(sig_earth_quakes,
#                        changes_earth_quakes,
#                        perfect_rois,
#                        save_fig=p('performance_earth_quakes_perfect.PNG'))
#

# def run_twi():
#     logger.info("Twi changes:")
#     logger.info(CHANGES_TWI)
#     predicted_rois = pccf_rois_adhoc(CHANGES_TWI, ROI_RADIUS_TWI)
#     logger.info("Twi predicted ROIs:")
#     for roi in predicted_rois:
#         logger.info(roi)
#     performance_signal(sig_twi,
#                        CHANGES_TWI,
#                        predicted_rois,
#                        save_fig=p('performance_twi.PNG'))
#
#     perfect_rois = get_perfect_rois(CHANGES_TWI, ROI_RADIUS_TWI)
#     logger.info("Twi perfect ROIs:")
#     for roi in perfect_rois:
#         logger.info(roi)
#     performance_signal(sig_twi,
#                        CHANGES_TWI,
#                        perfect_rois,
#                        save_fig=p('performance_twi_perfect.PNG'))


# def run_lake_level():
#     predicted_rois = pccf_rois_adhoc(changes_lake_level, ROI_RADIUS_LAKE)
#     performance_signal(sig_lake_level,
#                        changes_lake_level,
#                        predicted_rois,
#                        save_fig=p('performance_lake.PNG'))
#     perfect_rois = get_perfect_rois(changes_lake_level, ROI_RADIUS_LAKE)
#     performance_signal(sig_lake_level,
#                        changes_lake_level,
#                        perfect_rois,
#                        save_fig=p('performance_lake_perfect.PNG'))


def run_artificial_signal_not_sim_but_test():
    """
    The same as run_artificial_signal_avg() but single run and 2 changes.
    """
    n_points = 350
    sigma = 1.1
    mu0 = 0.0
    mu1 = 1.1
    sig = np.concatenate((randn(n_points) * sigma + mu0,
                          randn(n_points) * sigma + mu1,
                          randn(n_points) * sigma
                          ), axis=0)
    changes_art_sig = [n_points+1, 2*n_points+1]
    perfect_rois = get_perfect_rois(changes_art_sig,
                                    ROI_RADIUS_ARTIFICIAL_SIGNAL)
    performance_signal(sig,
                       changes_art_sig,
                       perfect_rois,
                       save_fig=p('performance_art_sig.PNG'))


def run_temperature_signal():
    perf = PccfPerformanceMetrics(changes_temperature)
    mu_vec, f1_vec = collect_pccf_performance(changes_temperature, mu_range=(950, 1350), radius_range=(300, 600))
    mu_opt = mu_vec[np.argmax(f1_vec)]
    pccf = Pccf(mu_opt, radius=500)
    # rois = pccf.roi_intervals(len(changes_temperature))
    rois = rois_temperature_signal()
    logger.info(f"Pccf F1 score for temperature signal ={perf.f1_score(rois)}")

    performance_signal(sig_temperature,
                       changes_temperature,
                       rois,
                       theta_vec=gen_theta_vec(0.1, 1, 800),
                       save_fig='../../tex/img/performance_temperature.eps')

    performance_signal(sig_temperature,
                       changes_temperature[:7],
                       rois,
                       theta_vec=gen_theta_vec(0.1, 1, 800),
                       save_fig='../../tex/img/performance_temperature_seven.eps')


if __name__ == "__main__":
    run_temperature_signal()
