"""
Common only for examples
"""
from dataclasses import dataclass


@dataclass
class SignalSettings:
    mu0: float
    mu1: float
    sigma: float
    half_len: int
