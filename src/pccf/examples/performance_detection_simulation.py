#
# Obtain smooth performance curves using artificial signals.
#
from pccf.detector import make_cusum_pccf, cusum, make_cusum_single_ref, \
    make_cusum_single_pccf_ref
from numpy.random import randn
from pccf.examples.proof_of_concept_console import results_example
from tqdm import tqdm
from pccf.detector_performance import *
from pccf.settings import *
from loguru import logger
from pccf.examples.common import SignalSettings
from pccf.utils_general import path_ext_low_with_dot
import matplotlib.pyplot as plt
import json
import tempfile

np.random.seed(1602)


def _run_single_alarm_cusum_sim(output_fig=None):
    """
    For these detectors there is always either exactly 1 TP or 1 FN,
    or no detections.
    In this simulation CUSUM alarms change and then loop is stopped.
    We don't continue running detector.
    """
    n_sim = 300
    n_points = 150
    sigma = 1.1
    mu0 = 0.0
    mu1 = 1.1
    ROI_RADIUS = 15
    changes = [n_points + 1]
    tps_vec_stat = []
    tps_vec_dyn = []
    fns_vec_stat = []
    fns_vec_dyn = []
    fps_vec_stat = []
    fps_vec_dyn = []
    delays_vec_stat = []
    delays_vec_dyn = []
    cusum_single_ref = make_cusum_single_ref(mu0)
    rois = [Roi(n_points, ROI_RADIUS)]
    cusum_single_pccf_ref = make_cusum_single_pccf_ref(mu0, Roi(n_points + 1,
                                                                ROI_RADIUS))
    theta_vec = gen_theta_vec(0.1, 0.3, 50)
    for _ in tqdm(range(n_sim)):
        sig = np.concatenate((randn(n_points) * sigma + mu0,
                              randn(n_points) * sigma + mu1), axis=0)
        # Vectors with counts
        cost_stat, delays_stat, fps_stat, fns_stat, tps_stat, f1_stat = \
            collect_performance(cusum_single_ref, sig, changes, theta_vec)
        cost_dyn, delays_dyn, fps_dyn, fns_dyn, tps_dyn, f1_dyn = \
            collect_performance(cusum_single_pccf_ref, sig, changes, theta_vec, rois=rois)
        assert fps_dyn == fns_dyn  # This is true because of how detector is
        # implemented for single change, it alarms and stops
        delays_vec_stat.append(delays_stat)
        delays_vec_dyn.append(delays_dyn)
        fps_vec_stat.append(fps_stat)
        fps_vec_dyn.append(fps_dyn)
        fns_vec_stat.append(fns_stat)
        fns_vec_dyn.append(fns_dyn)
        tps_vec_stat.append(tps_stat)
        tps_vec_dyn.append(tps_dyn)

    delays_stat_m = np.vstack(delays_vec_stat)
    delays_dyn_m = np.vstack(delays_vec_dyn)
    fps_stat_m = np.vstack(fps_vec_stat)
    fps_dyn_m = np.vstack(fps_vec_dyn)
    fns_stat_m = np.vstack(fns_vec_stat)
    fns_dyn_m = np.vstack(fns_vec_dyn)
    tps_stat_m = np.vstack(tps_vec_stat)
    tps_dyn_m = np.vstack(tps_vec_dyn)
    averaged_delays_stat = average_results_matrix_rows(delays_stat_m)
    averaged_delays_dyn = average_results_matrix_rows(delays_dyn_m)
    averaged_fps_stat = average_results_matrix_rows(fps_stat_m,
                                                    count_nans_as_zero=True)
    averaged_fps_dyn = average_results_matrix_rows(fps_dyn_m,
                                                   count_nans_as_zero=True)
    averaged_fns_stat = average_results_matrix_rows(fns_stat_m,
                                                    count_nans_as_zero=True)
    averaged_fns_dyn = average_results_matrix_rows(fns_dyn_m,
                                                   count_nans_as_zero=True)
    averaged_tps_stat = average_results_matrix_rows(tps_stat_m)
    averaged_tps_dyn = average_results_matrix_rows(tps_dyn_m)
    if 1 == 1:
        fig = plt.figure(144, figsize=(12, 4))
        ax1 = fig.add_subplot(141)
        ax1.plot(theta_vec, averaged_delays_stat, ls="-", color="black")
        ax1.plot(theta_vec, averaged_delays_dyn, ls="-", color="red")
        ax1.set_title('Detection delays')
        ax1.set_xlabel('Theta')
        ax1.set_ylabel('Average detection delay')
        ax2 = fig.add_subplot(142)
        ax2.plot(theta_vec, averaged_fps_stat, ls="-", color="black")
        ax2.plot(theta_vec, averaged_fps_dyn, ls="-", color="red")
        ax2.set_title('FPs number')
        ax2.set_xlabel('Theta')
        ax2.set_ylabel('Average FPS number')
        ax3 = fig.add_subplot(143)
        ax3.plot(theta_vec, averaged_fns_stat, ls="-", color="black")
        ax3.plot(theta_vec, averaged_fns_dyn, ls="-", color="red")
        ax3.set_title('FNs number')
        ax3.set_xlabel('Theta')
        ax3.set_ylabel('Average FNS number')
        ax4 = fig.add_subplot(144)
        ax4.plot(theta_vec, averaged_tps_stat, ls="-", color="black")
        ax4.plot(theta_vec, averaged_tps_dyn, ls="-", color="red")
        ax4.set_title('TPs number')
        ax4.set_xlabel('Theta')
        ax4.set_ylabel('Average TPs number')
        if output_fig:
            plt.savefig(output_fig, format='eps')
            logger.info(f"Save Figure into {output_fig}")
        else:
            plt.show()


@dataclass()
class SimulationResults:
    theta_vec: list
    averaged_delays_stat: list
    averaged_delays_dyn: list
    averaged_tps_stat: list
    averaged_tps_dyn: list
    averaged_fps_stat: list
    averaged_fps_dyn: list
    averaged_fns_stat: list
    averaged_fns_dyn: list
    averaged_f1_stat: list
    averaged_f1_dyn: list


@dataclass
class SampleSignalAndSettings:
    sig: np.ndarray
    changes: list
    rois: List[Roi]


def run_artificial_signal_avg(max_theta=10,
                              signal_settings: SignalSettings = None,
                              one_change_point=True):
    """
    The same as run_artificial_signal() in performance_detection_signals.py but
    averaged results and 1 change.
    """
    logger.info("Note: If we have multiple changes then late detections can "
                "start corresponding to next changes...")
    n_sim = 1000
    n_points = signal_settings.half_len
    logger.info(f"n_points={n_points}")
    sigma = signal_settings.sigma
    mu0 = signal_settings.mu0
    mu1 = signal_settings.mu1
    if one_change_point:
        changes = [n_points]
    else:
        changes = [n_points, 2 * n_points]

    rois = [Roi(change, ROI_RADIUS_ARTIFICIAL_SIGNAL) for change in changes]

    logger.info(f"Signal length={2*n_points}")
    logger.info("Changes:")
    logger.info(changes)
    logger.info("ROIs: ")
    logger.info(rois)
    logger.info(f"mu0={signal_settings.mu0}, mu1={signal_settings.mu1}, "
                f"sigma={signal_settings.sigma}")

    tps_vec_stat = []
    tps_vec_dyn = []

    fns_vec_stat = []
    fns_vec_dyn = []

    fps_vec_stat = []
    fps_vec_dyn = []

    delays_vec_stat = []
    delays_vec_dyn = []

    f1_vec_stat = []
    f1_vec_dyn = []

    theta_vec = gen_theta_vec(0.1, 1, max_theta)
    cusum_pccf_ref = make_cusum_pccf(rois)

    sig_and_settings: SampleSignalAndSettings = None

    for k in tqdm(range(n_sim)):
        if one_change_point:
            sig = np.concatenate((randn(n_points) * sigma + mu0,
                                  randn(n_points) * sigma + mu1,
                                  ), axis=0)
            if k == 0:
                sig_and_settings = SampleSignalAndSettings(sig, changes, rois)
        else:
            sig = np.concatenate((randn(n_points) * sigma + mu0,
                                  randn(n_points) * sigma + mu1,
                                  randn(n_points) * sigma + mu0
                                  ), axis=0)
        # Vectors with counts
        cost_stat, delays_stat, fps_stat, fns_stat, tps_stat, f1_stat = \
            collect_performance(cusum, sig, changes, theta_vec)

        cost_dyn, delays_dyn, fps_dyn, fns_dyn, tps_dyn, f1_dyn = \
            collect_performance(cusum_pccf_ref, sig, changes, theta_vec,
                                rois=rois)

        delays_vec_stat.append(delays_stat)
        delays_vec_dyn.append(delays_dyn)

        fps_vec_stat.append(fps_stat)
        fps_vec_dyn.append(fps_dyn)

        fns_vec_stat.append(fns_stat)
        fns_vec_dyn.append(fns_dyn)

        tps_vec_stat.append(tps_stat)
        tps_vec_dyn.append(tps_dyn)

        f1_vec_stat.append(f1_stat)
        f1_vec_dyn.append(f1_dyn)

    delays_stat_m = np.vstack(delays_vec_stat)
    delays_dyn_m = np.vstack(delays_vec_dyn)

    fps_stat_m = np.vstack(fps_vec_stat)
    fps_dyn_m = np.vstack(fps_vec_dyn)

    fns_stat_m = np.vstack(fns_vec_stat)
    fns_dyn_m = np.vstack(fns_vec_dyn)

    tps_stat_m = np.vstack(tps_vec_stat)
    tps_dyn_m = np.vstack(tps_vec_dyn)

    f1_stat_m = np.vstack(f1_vec_stat)
    f1_dyn_m = np.vstack(f1_vec_dyn)

    averaged_delays_stat = average_results_matrix_rows(delays_stat_m)
    averaged_delays_dyn = average_results_matrix_rows(delays_dyn_m)

    averaged_fps_stat = average_results_matrix_rows(fps_stat_m,
                                                    count_nans_as_zero=True)
    averaged_fps_dyn = average_results_matrix_rows(fps_dyn_m,
                                                   count_nans_as_zero=True)

    averaged_fns_stat = average_results_matrix_rows(fns_stat_m,
                                                    count_nans_as_zero=True)

    averaged_fns_dyn = average_results_matrix_rows(fns_dyn_m,
                                                   count_nans_as_zero=True)

    averaged_tps_stat = average_results_matrix_rows(tps_stat_m)
    averaged_tps_dyn = average_results_matrix_rows(tps_dyn_m)

    averaged_f1_stat = average_results_matrix_rows(f1_stat_m,
                                                   count_nans_as_zero=True)
    averaged_f1_dyn = average_results_matrix_rows(f1_dyn_m,
                                                  count_nans_as_zero=True)

    # Normalize
    averaged_fps_stat /= len(sig)
    averaged_fps_dyn /= len(sig)
    averaged_fns_stat /= len(changes)
    averaged_fns_dyn /= len(changes)
    averaged_tps_stat /= len(changes)
    averaged_tps_dyn /= len(changes)
    #
    averaged_f1_stat /= len(changes)
    averaged_f1_dyn /= len(changes)

    results = SimulationResults(theta_vec,
                                averaged_delays_stat, averaged_delays_dyn,
                                averaged_tps_stat, averaged_tps_dyn,
                                averaged_fps_stat, averaged_fps_dyn,
                                averaged_fns_stat, averaged_fns_dyn,
                                averaged_f1_stat, averaged_f1_dyn
                                )
    return results, sig_and_settings


def plot_simulation_results(results_temp_file,
                            output_fig=None):

    with open(results_temp_file, 'r') as fp:
        json_cont = json.load(fp)
        logger.info(f"Load results from {results_temp_file}")
    common_font_size = 15
    common_font_size_axis = 15
    common_line_width = 2
    common_tick_size = 15

    fig = plt.figure('art sig sim', figsize=(12, 14))

    ax1 = fig.add_subplot(321)
    ax2 = fig.add_subplot(322)
    ax3 = fig.add_subplot(323)
    ax4 = fig.add_subplot(324)
    ax5 = fig.add_subplot(325)
    ax6 = fig.add_subplot(326)

    r = json_cont

    def plot_delay_to_ax(ax_ref, r_item, title):
        theta_vec = r_item['theta_vec']
        ax_ref.plot(theta_vec, r_item['averaged_delays_stat'], ls="-", color="black", linewidth=common_line_width)
        ax_ref.plot(theta_vec, r_item['averaged_delays_dyn'], ls="--", color="black", linewidth=common_line_width)
        ax_ref.set_title(title, fontsize=common_font_size)
        ax_ref.set_xlabel('Threshold', fontsize=common_font_size_axis)
        ax_ref.set_ylabel('Average detection delay', fontsize=common_font_size_axis)
        ax_ref.xaxis.set_tick_params(labelsize=common_tick_size)
        ax_ref.yaxis.set_tick_params(labelsize=common_tick_size)
        ax_ref.xaxis.grid(True, linestyle='--')
        ax_ref.yaxis.grid(True, linestyle='--')

    plot_delay_to_ax(ax1, r[0], 'Detection delays for $\delta=1.1$')
    plot_delay_to_ax(ax3, r[1], 'Detection delays for $\delta=2.1$')
    plot_delay_to_ax(ax5, r[2], 'Detection delays for $\delta=3.1$')

    def plot_f1_to_ax(ax_ref, r_item, title):
        ax_ref.plot(r_item['theta_vec'], r_item['averaged_f1_stat'], ls="-", color="black", linewidth=common_line_width)
        ax_ref.plot(r_item['theta_vec'], r_item['averaged_f1_dyn'], ls="--", color="black", linewidth=common_line_width)
        ax_ref.set_title(title, fontsize=common_font_size)
        ax_ref.set_xlabel('Threshold', fontsize=common_font_size_axis)
        ax_ref.set_ylabel('F1 score', fontsize=common_font_size_axis)
        ax_ref.xaxis.set_tick_params(labelsize=common_tick_size)
        ax_ref.yaxis.set_tick_params(labelsize=common_tick_size)
        ax_ref.xaxis.grid(True, linestyle='--')
        ax_ref.yaxis.grid(True, linestyle='--')

    plot_f1_to_ax(ax2, r[0], 'F1 score for $\delta=1.1$')
    plot_f1_to_ax(ax4, r[1], 'F1 score for $\delta=2.1$')
    plot_f1_to_ax(ax6, r[2], 'F1 score for $\delta=3.1$')

    plt.tight_layout()
    if output_fig:
        if path_ext_low_with_dot(output_fig) == '.eps':
            plt.savefig(output_fig, format='eps')
        elif path_ext_low_with_dot(output_fig) == '.png':
            plt.savefig(output_fig, format='png', dpi=300)
        logger.info(f"Save Figure into {output_fig}")
        plt.close()
    else:
        plt.show()


def plot_signal_settings(res: SampleSignalAndSettings, output_fig=None):
    fig = plt.figure(111, figsize=(5, 4))
    ax1 = fig.add_subplot(111)
    ax1.plot(res.sig, color='black')
    for change in res.changes:
        ax1.axvline(change, color='black')
    for roi in res.rois:
        ax1.axvline(roi.left, color='red', ls='dashed')
        ax1.axvline(roi.center, color='red')
        ax1.axvline(roi.right, color='red', ls='dashed')
    if output_fig:
        plt.savefig(output_fig, format='eps')
        logger.info(f"Save signal sample into {output_fig}")
        plt.close()
    else:
        plt.show()


def run_simulation_for_mu2(mu2):
    """ To show how changes relation between performances """
    sig_settings = SignalSettings(0.0, mu2, 1.0, 100)
    results_example(sig_settings, 5.0)
    r, _ = run_artificial_signal_avg(max_theta=100,  # 40
                                     signal_settings=sig_settings,
                                     one_change_point=True)
    return r


def run_simulations_for_mu2s(mu2_list=[1.1, 2.1, 3.1]):
    results_ = []
    for mu2 in mu2_list:
        r = run_simulation_for_mu2(mu2)
        results_.append({'mu': mu2,
                         'theta_vec': list(r.theta_vec),
                         'averaged_delays_stat': list(r.averaged_delays_stat),
                         'averaged_delays_dyn': list(r.averaged_delays_dyn),
                         'averaged_f1_stat': list(r.averaged_f1_stat),
                         'averaged_f1_dyn': list(r.averaged_f1_dyn)
                         })
    with tempfile.NamedTemporaryFile(dir='./temp',
                                     prefix='performance_detection_sim_results',
                                     delete=False,
                                     mode='w') as fp:
        json.dump(results_, fp)
        save_path = fp.name
        logger.info(f"Save simulation results into {fp.name}")
    return save_path


if __name__ == "__main__":
    results_file = None
    if 1 == 0:
        results_file = run_simulations_for_mu2s()
    else:
        results_file = "/home/ms314/1/phd/src/pccf/temp/performance_detection_sim_resultsp_micfuw"
        results_file = "/home/ms314/1/phd/src/pccf/temp/performance_detection_sim_resultscmfwrwsv"
    plot_name = 'performance_detection_sim'
    logger.info("Plot EPS")
    plot_simulation_results(results_file, output_fig='../../tex/img/' + plot_name + '.eps')
    logger.info("Plot PNG")
    plot_simulation_results(results_file, output_fig='./img/' + plot_name + '.PNG')
