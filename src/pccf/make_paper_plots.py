"""
Make all plots for paper
"""

import subprocess


# subprocess.run(['python', 'examples/proof_of_concept1.py'], check=True)
# subprocess.run(['python', 'examples/proof_of_concept2.py'], check=True)
# subprocess.run(['python', 'examples/pccf_example.py'], check=True)
# RUN_COMPUTATIONALLY_EXPENSIVE_STEPS = True
# if RUN_COMPUTATIONALLY_EXPENSIVE_STEPS:
#     subprocess.run(['python', 'examples/performance_detection_simulation.py'], check=True)
# subprocess.run(['python', 'examples/performance_pccf_signals.py'], check=True)
subprocess.run(['python', 'examples/performance_detection_signals.py'], check=True)
subprocess.run(['python', 'proc_plot_temperature_signal.py'], check=True)

