import matplotlib.pyplot as plt
from pccf.data import *
from debug_temperature_data import *


def plot_temperature_signal(save_fig=''):
    fig = plt.figure(figsize=(24, 12))
    ax1 = fig.add_subplot(211)
    ax1.plot(sig_temperature, ls="-", color="black", linewidth=4)
    for c in changes_temperature:
        ax1.axvline(c, color="black")

    ax1.xaxis.set_tick_params(labelsize=30)
    ax1.yaxis.set_tick_params(labelsize=30)

    ax2 = fig.add_subplot(212)
    ax2.xaxis.set_tick_params(labelsize=30)
    ax2.yaxis.set_tick_params(labelsize=30)
    ax2.plot(probs[600:], color='black', linewidth=4)
    for r in rois_temperature:
        ax2.axvline(r.left, color='black', ls='--', linewidth=4)
        ax2.axvline(r.right, color='black', ls='--', linewidth=4)
    for c in changes_temperature:
        ax2.axvline(c, color='red', linewidth=4)

    if save_fig:
        if path_ext_low_with_dot(save_fig) == '.eps':
            plt.savefig(save_fig, format='eps')
        elif path_ext_low_with_dot(save_fig) == '.png':
            plt.savefig(save_fig, format='png', dpi=300)
        else:
            raise NotImplementedError
        logger.info(f"Save Figure into {save_fig}")
        plt.close(fig)


if __name__ == "__main__":
    plot_temperature_signal(save_fig='../../tex/img/temperature_signal.eps')