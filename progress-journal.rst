- Add pseudocode (?)
- Find optimal Pccf for every signal - peak 2-3 best
- Delay can be small but FN rate can be high (e.g. one change is only detected)


To see what I have been doing `git log --stat -n5`

Daily
========


2020-12-08:

Calculated F1.


2020-11-21:


Results from diagnostics function look good.
At the same time simulation figures are not so good.


- Count FPs / length of signal
- FNs and TPs are alrady normalized



2020-11-19:

  Yesterday - meeting with Nikolai.
  - Fix artificial signal results simulation - do same as for single run but average.
  - Fix FP rate near zero - it seems it is 0
  - bad CUSUM and bad Pccf connection
  - when Pccf is bad examples ?
  - TODO: Adwin? Pccf updates?


  Why delay is ok but FN is high and TP is low



2020-11-18:

Performance for artificial signals - simulation and with 2 changes seems to be OK.
Fixed a lot in perf.py, added much more tests.
Figure for artificial signal look good, but for real signals not so good.


2020-11-17:

  `test_collect_performance.py`


2020-11-16:


  Error: cde within next ROI is attributed to previous ROI when calculating delays.

  `python3 examples/performance_signals.py`


  Good consistent improvement in a narrow settings window for artifciaial signal.


  Do 2 cases - automatically predicted and when we make ROIs artificially
  correct.

  Re-do twi signal results.


2020-11-15:

  We predict ROI centers using optimization step.
  But widths are calculated ad-hoc.


  Why sometimes detection delay is bigger with dynamic settings for Earth
  quakes signal??



2020-11-14:

  Plot Pccf on top of signals and ROIs.
  `src/pccf/examples/optim_pccf_signals_changes.py`

  working on corresponding performance plot
  `performance_signal_earth_quakes`

  Eart quakes signal: why detection delay is larger? It should be not possible.


2020-11-12:

    `examples/optim_pccf_signals_changes.py`


2020-11-06:

  Add proof of concept case when there is FN


2020-11-05:

  Add FN plots.
      FNS number is bigger on average, BUT it is very small.


2020-11-03:

  Fixed `examples/artificial_signal_oerf_stats.py`


2020-10-27:
2020-10-25:
    Obtain smooth curves for detection delay and FA.
    Add artificial signal results for delay and FA rate.

2020-10-20:
    Add PCCF plot.

2020-10-19:
    Add figure explaining the method. ``python3 examples/proof_of_concept2.py``
